"""
CIS 211 W19 Project
Duck Machine

Author: Cameron Jordal

Credits: N/A


Duck Machine model DM2019W CPU
"""
# our modules
from instr_format import Instruction, OpCode, CondFlag, decode
# ready-made modules
from typing import Tuple
from memory import Memory
from register import Register, ZeroRegister
from mvc import MVCEvent, MVCListenable
# debugging modules
import logging
logging.basicConfig()
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)


class ALU(object):
    """The arithmetic logic unit (also called a "functional unit"
    in a modern CPU) executes a selected function but does not
    otherwise manage CPU state. A modern CPU core may have several
    ALUs to boost performance by performing multiple operatons
    in parallel, but the Duck Machine has just one ALU in one core.
    """
    # The ALU chooses one operation to apply based on a provided
    # operation code.  These are just simple functions of two arguments;
    # in hardware we would use a multiplexer circuit to connect the
    # inputs and output to the selected circuitry for each operation.
    ALU_OPS = {
        OpCode.ADD: lambda x, y: x + y,
        OpCode.SUB: lambda x, y: x - y,
        OpCode.MUL: lambda x, y: x * y,
        OpCode.DIV: lambda x, y: x // y,
        # For memory access operations load, store, the ALU
        # performs the address calculation
        OpCode.LOAD: lambda x, y: x + y,
        OpCode.STORE: lambda x, y: x + y,
        # Some operations perform no operation
        OpCode.HALT: lambda x, y: 0
    }

    def exec(self, op: OpCode, in1: int, in2: int) -> Tuple[int, CondFlag]:
        # compute operation
        operation = self.ALU_OPS[op]
        try:
            result = operation(in1, in2)
        # overflow result
        except ZeroDivisionError or OverflowError:
            return 0, CondFlag.V
        # zero result
        if result == 0:
            return 0, CondFlag.Z
        # positive result
        if result > 0:
            return result, CondFlag.P
        # negative result
        if result < 0:
            return result, CondFlag.M


class CPUStep(MVCEvent):
    """CPU is beginning step with PC at a given address"""
    def __init__(self, subject: "CPU", pc_addr: int,
                 instr_word: int, instr: Instruction)-> None:
        self.subject = subject
        self.pc_addr = pc_addr
        self.instr_word = instr_word
        self.instr = instr


class CPU(MVCListenable):
    """Duck Machine central processing unit (CPU)
    has 16 registers (including r0 that always holds zero
    and r15 that holds the program counter), a few
    flag registers (condition codes, halted state),
    and some logic for sequencing execution.  The CPU
    does not contain the main memory but has a bus connecting
    it to a separate memory.
    """

    def __init__(self, memory: Memory):
        super().__init__()
        self.memory = memory  # Not part of CPU; what we really have is a connection
        self.registers = [ZeroRegister(), Register(), Register(), Register(),
                          Register(), Register(), Register(), Register(),
                          Register(), Register(), Register(), Register(),
                          Register(), Register(), Register(), Register()]
        self.condition = CondFlag.ALWAYS
        self.halted = False
        self.alu = ALU()
        self.pc = self.registers[15]

    def _get_reg_val(self, reg_id):
        """get the value of a specified register"""
        register = self.registers[reg_id]
        content = register.get()
        return content

    def _set_reg_val(self, reg_id, value):
        """set the value of a specified register to a specified value"""
        register = self.registers[reg_id]
        register.put(value)

    def step(self):
        # fetch
        instr_addr = self.pc.get()
        instr_word = self.memory.get(instr_addr)
        # decode
        instr = decode(instr_word)
        # Display the CPU state when we have decoded the instruction,
        # before we have executed it
        self.notify_all(CPUStep(self, instr_addr, instr_word, instr))
        # execute
        condition = self.condition & instr.cond
        if condition.value > 0:
            # get value from source registers
            src1_val = self._get_reg_val(instr.reg_src1)
            src2_val = self._get_reg_val(instr.reg_src2)
            # set values of operands for ALU
            left = src1_val
            right = src2_val + instr.offset
            # execute specified operation
            result_value = self.alu.exec(instr.op, left, right)
            # increment program counter
            self.pc.put(instr_addr + 1)
            # complete specified operation
            if instr.op == OpCode.STORE:
                # take value at a target register and put
                # it at computed memory location
                target_val = self._get_reg_val(instr.reg_target)
                self.memory.put(result_value[0], target_val)
            if instr.op == OpCode.LOAD:
                # set target register to value of computed memory location
                result_mem_value = self.memory.get(result_value[0])
                self._set_reg_val(instr.reg_target, result_mem_value)
            if instr.op == OpCode.HALT:
                # stop cpu after this step
                self.halted = True
            if instr.op in [OpCode.ADD, OpCode.SUB,
                            OpCode.MUL, OpCode.DIV]:
                # set target register to result value
                self._set_reg_val(instr.reg_target, result_value[0])
                # set new CPU condition
                self.condition = result_value[1]
        else:
            self.pc.put(instr_addr + 1)

    def run(self, from_addr=0, single_step=False) -> None:
        self.halted = False
        self.pc.put(from_addr)
        step_count = 0
        while not self.halted:
            if single_step:
                input("Step {}; press enter".format(step_count))
            self.step()
            step_count += 1
